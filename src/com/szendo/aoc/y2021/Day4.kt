package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/4.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .split("")

    val numbers = input[0][0].split(",").map { it.toInt() }
    val boards = input.drop(1).map { b ->
        b.map { row -> row.trim().split(Regex("\\s+")).map { it.toInt() to false }.toMutableList() }
    }.toMutableList()

    fun isWinner(board: List<List<Pair<Int, Boolean>>>) =
        (0..4).any { a ->
            (0..4).all { b -> board[a][b].second } || (0..4).all { b -> board[b][a].second }
        }

    fun getScore(board: List<List<Pair<Int, Boolean>>>, n: Int) =
        board.sumOf { row -> row.sumOf { if (it.second) 0 else it.first } } * n

    val answer1 = run {
        for (n in numbers) {
            for (b in boards) {
                for (x in 0..4) {
                    for (y in 0..4) {
                        if (b[x][y].first == n) {
                            b[x][y] = n to true
                            if (isWinner(b)) {
                                return@run getScore(b, n)
                            }
                        }
                    }
                }
            }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        for (n in numbers) {
            val iterator = boards.iterator()
            boards@ while (iterator.hasNext()) {
                val b = iterator.next()
                for (x in 0..4) {
                    for (y in 0..4) {
                        if (b[x][y].first == n) {
                            b[x][y] = n to true
                            if (isWinner(b)) {
                                if (boards.size == 1) {
                                    return@run getScore(boards[0], n)
                                }
                                iterator.remove()
                                continue@boards
                            }
                        }
                    }
                }
            }
        }
    }
    println("Part 2: $answer2")
}
