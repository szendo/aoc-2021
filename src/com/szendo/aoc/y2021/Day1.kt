package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/1.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toInt() }

    val answer1 = run {
        input.windowed(2).count { it[0] < it[1] }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.windowed(4).count { it[0] < it[3] }
    }
    println("Part 2: $answer2")
}
