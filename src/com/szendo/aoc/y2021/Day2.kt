package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/2.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map {
            val (dir, dist) = it.split(' ')
            when (dir) {
                "up" -> 0L to -(dist.toLong())
                "down" -> 0L to (dist.toLong())
                "forward" -> (dist.toLong()) to 0L
                else -> error("Unknown direction: $dir")
            }
        }

    val answer1 = run {
        val finalPos = input.reduce { a, b -> (a.first + b.first) to (a.second + b.second) }
        finalPos.first * finalPos.second
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var pos = 0L to 0L
        var aim = 0L

        for (dpos in input) {
            aim += dpos.second
            pos = pos.first + dpos.first to pos.second + (dpos.first * aim)
        }

        pos.first * pos.second
    }
    println("Part 2: $answer2")
}
