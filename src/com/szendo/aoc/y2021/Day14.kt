package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/14.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .split("")

    val template = input[0][0].toList()

    val rules = input[1].associate { line ->
        line.split(" -> ", limit = 2).let { it[0].toList() to it[1][0] }
    }

    val answer1 = run {
        val counts = (1..10).fold(template) { polymer, _ ->
            polymer.windowed(2, partialWindows = true)
                .flatMap { pair ->
                    if (pair in rules) {
                        listOf(pair[0], rules.getValue(pair))
                    } else {
                        pair
                    }
                }
        }.groupingBy { it }.eachCount()

        counts.values.maxOf { it } - counts.values.minOf { it }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        var countsByPair = template.windowed(2).groupingBy { it }.eachCount().mapValues { it.value.toLong() }

        repeat(40) {
            val newCountsByPair = mutableMapOf<List<Char>, Long>()
            countsByPair.forEach { (pair, count) ->
                val element = rules.getValue(pair)
                newCountsByPair.merge(listOf(pair[0], element), count, Long::plus)
                newCountsByPair.merge(listOf(element, pair[1]), count, Long::plus)
            }
            countsByPair = newCountsByPair
        }

        val countsByElement = mutableMapOf<Char, Long>()
        countsByElement[template.first()] = 1
        countsByElement[template.last()] = 1
        countsByPair.forEach { (pair, count) ->
            countsByElement.merge(pair[0], count, Long::plus)
            countsByElement.merge(pair[1], count, Long::plus)
        }
        (countsByElement.maxOf { it.value } - countsByElement.minOf { it.value }) / 2
    }
    println("Part 2: $answer2")
}
