package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/12.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { row -> row.split('-', limit = 2) }
        .flatMap { listOf(it[0] to it[1], it[1] to it[0]) }
        .groupBy({ it.first }) { it.second }

    val answer1 = run {
        val paths = mutableSetOf<List<String>>()
        val queue = mutableListOf(listOf("start"))
        while (queue.isNotEmpty()) {
            val path = queue.removeFirst()
            val last = path.last()

            if (last == "end") {
                paths.add(path)
            } else {
                for (next in input.getValue(last)) {
                    if (next == next.uppercase() || next !in path) {
                        queue.add(path + next)
                    }
                }
            }
        }
        paths.size
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val paths = mutableSetOf<List<String>>()
        val queue = mutableListOf(listOf("start") to false)
        while (queue.isNotEmpty()) {
            val (path, twice) = queue.removeFirst()
            val last = path.last()

            if (last == "end") {
                paths.add(path)
            } else {
                for (next in input.getValue(last)) {
                    if (next == next.uppercase() || next !in path) {
                        queue.add(path + next to twice)
                    } else if (!twice && next != "start") {
                        queue.add(path + next to true)
                    }
                }
            }
        }
        paths.size
    }
    println("Part 2: $answer2")
}
