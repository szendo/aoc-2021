package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/10.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val answer1 = run {
        val points = mapOf(
            ')' to 3,
            ']' to 57,
            '}' to 1197,
            '>' to 25137,
        )

        input.sumOf { line ->
            val stack = mutableListOf<Char>()
            for (c in line) {
                when (c) {
                    '(', '[', '{', '<' -> stack.add(c)
                    ')', ']', '}', '>' -> {
                        if (stack.isEmpty())
                            return@sumOf points[c]!!
                        val p = stack.removeLast()
                        if (c == ')' && p != '(' || c == ']' && p != '[' || c == '}' && p != '{' || c == '>' && p != '<') {
                            return@sumOf points[c]!!
                        }
                    }
                    else -> error("Invalid character: $c")
                }
            }
            0
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val points = mapOf(
            '(' to 1,
            '[' to 2,
            '{' to 3,
            '<' to 4,
        )

        input.fold(listOf<Long>()) { scores, line ->
            val stack = mutableListOf<Char>()
            for (c in line) {
                when (c) {
                    '(', '[', '{', '<' -> stack.add(c)
                    ')', ']', '}', '>' -> {
                        if (stack.isEmpty())
                            return@fold scores
                        val p = stack.removeLast()
                        if (c == ')' && p != '(' || c == ']' && p != '[' || c == '}' && p != '{' || c == '>' && p != '<') {
                            return@fold scores
                        }
                    }
                    else -> error("Invalid character: $c")
                }
            }
            scores + stack.foldRight(0L) { c, score -> 5 * score + points[c]!! }
        }.sorted().let { it[it.lastIndex / 2] }
    }
    println("Part 2: $answer2")
}
