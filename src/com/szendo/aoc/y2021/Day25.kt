package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/25.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { it.toList() }

    val answer1 = run {
        var grid = input

        var steps = 0L
        while (true) {
            steps++
            val gridAfterEastMove = grid.map { row ->
                row.mapIndexed { x, c ->
                    when (c) {
                        '>' -> if (row[(x + 1) % row.size] == '.') '.' else c
                        '.' -> if (row[Math.floorMod(x - 1, row.size)] == '>') '>' else c
                        else -> c
                    }
                }
            }
            val gridAfterSouthMove = gridAfterEastMove.mapIndexed { y, row ->
                row.mapIndexed { x, c ->
                    when (c) {
                        'v' -> if (gridAfterEastMove[(y + 1) % gridAfterEastMove.size][x] == '.') '.' else c
                        '.' -> if (gridAfterEastMove[Math.floorMod(y - 1, gridAfterEastMove.size)][x] == 'v') 'v' else c
                        else -> c
                    }
                }
            }
            if (grid == gridAfterSouthMove) return@run steps
            grid = gridAfterSouthMove
        }
    }
    println("Part 1: $answer1")
}
