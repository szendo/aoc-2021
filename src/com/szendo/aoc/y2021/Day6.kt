package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/6.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .split(',').map { it.toInt() }

    fun <T> stepSimulation(timers: MutableMap<Int, T>, adder: (a: T, B: T) -> T) {
        for (n in (0..8)) {
            if (timers.containsKey(n)) {
                timers[n - 1] = timers.remove(n)!!
            }
        }

        if (timers.containsKey(-1)) {
            val n = timers.remove(-1)!!
            timers[8] = n
            timers.merge(6, n, adder)
        }
    }

    val answer1 = run {
        val timers = input.groupingBy { it }
            .eachCount()
            .toMutableMap()
        repeat(80) { stepSimulation(timers) { a, b -> a + b } }
        timers.values.sum()
    }
    println("Part 1: $answer1")

    val answer2 = run {
        val timers = input.groupingBy { it }
            .eachCount()
            .mapValues { it.value.toLong() }
            .toMutableMap()
        repeat(256) { stepSimulation(timers) { a, b -> a + b } }
        timers.values.sum()
    }
    println("Part 2: $answer2")
}
