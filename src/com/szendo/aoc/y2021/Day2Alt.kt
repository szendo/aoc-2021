package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/2.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map {
            val (dir, dist) = it.split(' ')
            when (dir) {
                "up" -> 0 to -(dist.toInt())
                "down" -> 0 to (dist.toInt())
                "forward" -> (dist.toInt()) to 0
                else -> error("Unknown direction: $dir")
            }
        }

    val answer1 = run {
        input.reduce { a, b -> a + b }.let { it.x * it.y }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.fold(Triple(0L, 0L, 0L)) { (pos, depth, aim), (dpos, ddepth) ->
            Triple(pos + dpos, depth + dpos * aim, aim + ddepth)
        }.let { it.first * it.second }
    }
    println("Part 2: $answer2")
}
