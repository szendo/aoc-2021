package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/8.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map {
            it.split(" | ").let { row ->
                row[0].split(' ') to row[1].split(' ')
            }
        }

    val answer1 = run {
        input.sumOf { (_, output) ->
            output.count { it.length in setOf(2, 4, 3, 7) }
        }
    }
    println("Part 1: $answer1")

    val answer2 = run {
        input.sumOf { (samples, output) ->
            val d1 = samples.first { it.length == 2 }.toSet()
            val d4 = samples.first { it.length == 4 }.toSet()
            val d7 = samples.first { it.length == 3 }.toSet()
            val d8 = samples.first { it.length == 7 }.toSet()
            val d235 = samples.filter { it.length == 5 }.map { it.toSet() }
            val d069 = samples.filter { it.length == 6 }.map { it.toSet() }
            val sBD = d4 - d1
            val d6 = d069.first { !it.containsAll(d1) }
            val sC = d1 - d6
            val sF = d1 - sC
            val d3 = d235.first { it.containsAll(d1) }
            val d25 = d235 - setOf(d3)
            val d2 = d25.first { it.containsAll(sC) }
            val d5 = d25.first { it.containsAll(sF) }
            val d09 = d069 - setOf(d6)
            val d9 = d09.first { it.containsAll(sBD) }
            val d0 = d09.first { !it.containsAll(sBD) }
            val digits = listOf(d0, d1, d2, d3, d4, d5, d6, d7, d8, d9)
            output.map { digit ->
                digits.indexOf(digit.toSet())
            }.reduce { a, b -> 10 * a + b }
        }
    }
    println("Part 2: $answer2")
}
