package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/3.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }

    val answer1 = run {
        val vals = input.map {
            it.toCharArray().toList().map { c -> if (c == '1') 1 else -1 }
        }.reduce { a, b -> a.zip(b) { x, y -> x + y } }

        val gamma = Integer.parseInt(vals.joinToString(separator = "") {
            (if (it >= 0) "1" else "0")
        }, 2)
        val epsilon = Integer.parseInt(vals.joinToString(separator = "") {
            (if (it >= 0) "0" else "1")
        }, 2)

        gamma * epsilon
    }
    println("Part 1: $answer1")

    val answer2 = run {
        tailrec fun process(values: List<String>, mostCommon: Boolean, prefix: String = ""): Pair<List<String>, String> {
            if (values.size == 1) {
                return emptyList<String>() to prefix + values[0]
            }

            if (values[0].isEmpty()) {
                return emptyList<String>() to prefix
            }

            val zeros = values.count { it[0] == '0' }
            val ones = values.size - zeros

            return if (if (mostCommon) (ones >= zeros) else (ones < zeros)) {
                process(
                    values.filter { it[0] == '1' }.map { it.substring(1) },
                    mostCommon,
                    prefix + "1"
                )
            } else {
                process(
                    values.filter { it[0] == '0' }.map { it.substring(1) },
                    mostCommon,
                    prefix + "0"
                )
            }
        }

        val o2GeneratorRating = Integer.parseInt(process(input, true).second, 2)
        val co2ScrubberRating = Integer.parseInt(process(input, false).second, 2)

        o2GeneratorRating * co2ScrubberRating
    }
    println("Part 2: $answer2")
}
