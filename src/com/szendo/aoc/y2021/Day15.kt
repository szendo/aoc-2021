package com.szendo.aoc.y2021

import com.szendo.aoc.*
import java.util.*

fun main() {
    val input = Helper.getResourceAsStream("2021/15.txt")
        .bufferedReader()
        .useLines { lines -> lines.toList() }
        .map { row -> row.toList().map { it.digitToInt() } }

    fun calculateLowestTotalRisk(riskLevel: List<List<Int>>): Int {
        val totalRiskLevel = riskLevel.map { row ->
            row.map { 10000 }.toMutableList()
        }
        totalRiskLevel.last()[totalRiskLevel.last().lastIndex] = riskLevel.last()[riskLevel.lastIndex]

        val queue = LinkedList(listOf(totalRiskLevel.last().lastIndex to totalRiskLevel.lastIndex))
        while (queue.isNotEmpty()) {
            val (x, y) = queue.removeFirst()
            listOf((0 to 1), (0 to -1), (1 to 0), (-1 to 0)).forEach { (dx, dy) ->
                try {
                    if (totalRiskLevel[y][x] + riskLevel[y + dy][x + dx] < totalRiskLevel[y + dy][x + dx]) {
                        totalRiskLevel[y + dy][x + dx] = totalRiskLevel[y][x] + riskLevel[y + dy][x + dx]
                        queue.add(x + dx to y + dy)
                    }
                } catch (ignored: IndexOutOfBoundsException) {
                }
            }
        }

        return totalRiskLevel[0][0] - riskLevel[0][0]
    }

    val answer1 = run {
        calculateLowestTotalRisk(input)
    }
    println("Part 1: $answer1")

    val answer2 = run {
        calculateLowestTotalRisk((0..4).flatMap { dy ->
            input.map { row ->
                (0..4).flatMap { dx ->
                    row.map { (it - 1 + dx + dy) % 9 + 1 }

                }
            }
        })
    }
    println("Part 2: $answer2")
}
