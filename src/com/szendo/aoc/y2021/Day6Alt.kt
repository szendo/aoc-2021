package com.szendo.aoc.y2021

import com.szendo.aoc.*

fun main() {
    val input = Helper.getResourceAsStream("2021/6.txt")
        .bufferedReader()
        .useLines { lines -> lines.first() }
        .split(',').map { it.toInt() }
        .groupingBy { it }
        .eachCount()

    val initialTimers = (0..8).map { input[it]?.toLong() ?: 0L }

    fun step(timers: List<Long>) = timers.indices.map {
        when (it) {
            8 -> timers[0]
            6 -> timers[7] + timers[0]
            else -> timers[it + 1]
        }
    }

    val answer1 = run {
        (1..80).fold(initialTimers) { timers, _ -> step(timers) }.sum()
    }
    println("Part 1: $answer1")

    val answer2 = run {
        (1..256).fold(initialTimers) { timers, _ -> step(timers) }.sum()
    }
    println("Part 2: $answer2")
}
