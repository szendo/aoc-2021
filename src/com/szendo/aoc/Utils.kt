package com.szendo.aoc

import java.math.BigInteger
import kotlin.math.abs

tailrec fun gcd(a: Int, b: Int): Int = if (a % b == 0) b else gcd(b, a % b)
tailrec fun gcd(a: Long, b: Long): Long = if (a % b == 0L) b else gcd(b, a % b)
tailrec fun gcd(a: BigInteger, b: BigInteger): BigInteger = if (a % b == BigInteger.ZERO) b else gcd(b, a % b)
fun lcm(a: Int, b: Int): Int = a / gcd(a, b) * b
fun lcm(a: Long, b: Long): Long = a / gcd(a, b) * b
fun lcm(a: BigInteger, b: BigInteger): BigInteger = a / gcd(a, b) * b

typealias Coord = Pair<Int, Int>
inline val Coord.x: Int get() = first
inline val Coord.y: Int get() = second
operator fun Coord.plus(other: Coord) = (x + other.x) to (y + other.y)
operator fun Coord.minus(other: Coord) = (x - other.x) to (y - other.y)
operator fun Coord.unaryMinus() = -x to -y
operator fun Coord.times(d: Int): Coord = (x * d) to (y * d)
operator fun Int.times(coord: Coord): Coord = (this * coord.x) to (this * coord.y)
fun Coord.distance(other: Coord) = abs(x - other.x) + abs(y - other.y)
fun Coord.rotateCCW() = -y to x
fun Coord.rotateCW() = y to -x

typealias Coord3D = Triple<Int, Int, Int>
inline val Coord3D.x: Int get() = first
inline val Coord3D.y: Int get() = second
inline val Coord3D.z: Int get() = third
operator fun Coord3D.plus(other: Coord3D) = Coord3D(x + other.x, y + other.y, z + other.z)
operator fun Coord3D.minus(other: Coord3D) = Coord3D(x - other.x, y - other.y, z - other.z)
operator fun Coord3D.unaryMinus() = Coord3D(-x, -y, -z)
operator fun Coord3D.times(d: Int) = Coord3D(d * x, d * y, d * z)
operator fun Int.times(coord: Coord3D) = Coord3D(this * coord.x, this * coord.y, this * coord.z)
fun Coord3D.distance(other: Coord3D) = abs(x - other.x) + abs(y - other.y) + abs(z - other.z)

data class Coord4D(val x: Int, val y: Int, val z: Int, val m: Int) {
    operator fun plus(other: Coord4D) = Coord4D(x + other.x, y + other.y, z + other.z, m + other.m)
    operator fun minus(other: Coord4D) = Coord4D(x + other.x, y + other.y, z + other.z, m + other.m)
    operator fun unaryMinus() = Coord4D(-x, -y, -z, -m)
    operator fun times(d: Int) = Coord4D(d * x, d * y, d * z, d * m)
    operator fun Int.times(coord: Coord4D) = Coord4D(this * coord.x, this * coord.y, this * coord.z, this * coord.m)
    fun distance(other: Coord4D) = abs(x - other.x) + abs(y - other.y) + abs(z - other.z) + abs(m - other.m)
}

typealias Grid<T> = List<List<T>>
operator fun <T> Grid<T>.get(coord: Coord) = this[coord.y][coord.x]

fun <T> Sequence<T>.split(delimiter: T): List<List<T>> {
    val result = mutableListOf<List<T>>()
    var current = mutableListOf<T>()
    for (e in this) {
        if (e == delimiter) {
            result.add(current)
            current = mutableListOf()
        } else {
            current.add(e)
        }
    }
    result.add(current)
    return result.toList()
}

fun <T> List<T>.split(delimiter: T): List<List<T>> = asSequence().split(delimiter)

fun <T> Sequence<T>.rle(): Sequence<Pair<T, Int>> {
    return sequence {
        val iterator = iterator()
        if (!iterator.hasNext()) {
            return@sequence
        }
        var current = iterator.next()
        var runLength = 1
        while (iterator.hasNext()) {
            val next = iterator.next()
            if (next == current) {
                runLength++
            } else {
                yield(current to runLength)
                current = next
                runLength = 1
            }
        }
        yield(current to runLength)
    }
}

fun <T> List<T>.rle() = asSequence().rle().toList()

infix fun Int.forLen(length: Int) = this until this + length
operator fun String.get(r: IntRange) = substring(r)
