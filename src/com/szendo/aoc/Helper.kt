package com.szendo.aoc

object Helper {
    fun getResourceAsStream(name: String) =
        requireNotNull(javaClass.getResourceAsStream(name)) { "Resource not found: $name" }
}
